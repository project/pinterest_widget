<?php

namespace Drupal\pinterest_widget\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Social Autopost.
 */
class SaveButtonSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['pinterest_save_button.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pinterest_widget.form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pinterest_save_button.settings');

    $form['advanced_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Default Configurations'),
      '#open' => TRUE,
      '#description' => $this->t('The default values will be auto populated on the fields and blocks.'),
    ];

    $button_types = [
      'round' => "Round",
      'large' => "Large",
    ];

    $languages = [
      "default" => $this->t("User's Default Language"),
      "en" => $this->t("English"),
      "cs" => $this->t("Czech"),
      "da" => $this->t("Danish"),
      "de" => $this->t("German"),
      "el" => $this->t("Greek"),
      "es" => $this->t("Spanish"),
      "fi" => $this->t("Finnish"),
      "fr" => $this->t("French"),
      "hi" => $this->t("Hindu"),
      "hu" => $this->t("Hungarian"),
      "id" => $this->t("Indonesian"),
      "it" => $this->t("Italian"),
      "ja" => $this->t("Japanese"),
      "ko" => $this->t("Korean"),
      "ms" => $this->t("Malaysian"),
      "nb" => $this->t("Norwegian"),
      "nl" => $this->t("Dutch"),
      "pl" => $this->t("Polish"),
      "pt" => $this->t("Portuguese"),
      "pt-br" => $this->t("Portuguese (Brazil)"),
      "ro" => $this->t("Romanian"),
      "ru" => $this->t("Russian"),
      "sk" => $this->t("Slovak"),
      "sv" => $this->t("Swedish"),
      "tl" => $this->t("Tagalog"),
      "th" => $this->t("Thai"),
      "tr" => $this->t("Turkish"),
      "uk" => $this->t("Ukrainian"),
      "vi" => $this->t("Vietnamese"),
    ];

    $selected_button_types = $config->get('button_types') ?? [];
    $language = $config->get('language') ?? [];

    $form['advanced_settings']['button_types'] = [
      '#type' => 'checkboxes',
      '#options' => $button_types,
      '#title' => $this->t('Button Types'),
      '#default_value' => $selected_button_types,
      '#description' => $this->t('Select available opions for user subscription'),
    ];
    $form['advanced_settings']['language'] = [
      '#type' => 'select',
      '#options' => $languages,
      '#title' => $this->t('Language'),
      '#default_value' => $language,
      '#description' => $this->t("User's Default Language will show a user the Save button in their own language. If their language is not supported, it will show in English. You can choose a language from the dropdown to always show the Save button in that language."),
    ];
    $form['advanced_settings']['language'] = [
      '#type' => 'select',
      '#options' => $languages,
      '#title' => $this->t('Language'),
      '#default_value' => $language,
      '#description' => $this->t("User's Default Language will show a user the Save button in their own language. If their language is not supported, it will show in English. You can choose a language from the dropdown to always show the Save button in that language."),
    ];

    $form['advanced_settings']['preview'] = [
      "#title" => $this->t('Preview'),
      "#type" => "item" ,
      "#markup" => '<img height="239" width="320" src="https://farm8.staticflickr.com/7027/6851755809_df5b2051c9_z.jpg">',
    ];

    $form['#attached']['library'][] = 'pinterest_widget/pinterest_widget';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $config_data = $this->config('pinterest_save_button.settings')
      ->set('button_types', $values['button_types'])
      ->set('language', $values['language']);

    $config_data->save();

    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

}
