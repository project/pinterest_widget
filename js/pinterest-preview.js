(function ($, Drupal, drupalSettings) {
    Drupal.behaviors.pinterest_behavior = {
        attach: function (context, settings) {
            $(document, context).once('pinterest_behavior').each( function () {

                $('#edit-pinterest-user-url').keyup(function () {
                    var dataPinHref = $('#edit-preview span').attr('data-pin-href');
                    let result = dataPinHref.replace("https://www.pinterest.com/pinterest", $(this).val());

                    $('#edit-preview span').attr('data-pin-href', result);
                });

                $('#edit-full-name').keyup(function () {
                    $('#edit-preview span').text($(this).val());
                });

                $('#edit-pin-url').keyup(function () {
                    $('#edit-preview span').text($(this).val());
                });

                $('#edit-pin-size').change(function () {

                    $('#edit-preview span').attr('data-pin-width', $(this).val());
                });

            });
        }
    }
} (jQuery, Drupal, drupalSettings));
