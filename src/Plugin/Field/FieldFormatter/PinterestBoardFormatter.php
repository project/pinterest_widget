<?php

namespace Drupal\pinterest_widget\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'pinterest_board_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "pinterest_board_formatter",
 *   label = @Translation("Pinterest board field formatter"),
 *   field_types = {
 *     "pinterest_board_fieldtype"
 *   }
 * )
 */
class PinterestBoardFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_featured_image) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_featured_image);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // The ProcessedText element already handles cache context & tag bubbling.
    // @see \Drupal\filter\Element\ProcessedText::preRenderText()
    foreach ($items as $delta => $item) {
      $values = $item->getValue();

      if (!empty($values)) {
        $pinterest_board_url = (isset($values['pinterest_board_url'])) ? $values['pinterest_board_url'] : '';

        if ($pinterest_board_url != '') {
          $size = (isset($values['size'])) ? $values['size'] : '';

          switch ($size) {
            case 'sidebar':
              $datap_in_board_width = "150";
              $data_pin_scale_height = "800";
              $data_pin_scale_width = "60";

              break;

            case 'header':
              $datap_in_board_width = "900";
              $data_pin_scale_height = "120";
              $data_pin_scale_width = "115";

              break;

            case 'custom':
              $datap_in_board_width = (isset($values['image_width'])) ? $values['image_width'] : '400';
              $data_pin_scale_height = (isset($values['board_height'])) ? $values['board_height'] : '240';
              $data_pin_scale_width = (isset($values['board_width'])) ? $values['board_width'] : '80';
              break;

            default:
              $datap_in_board_width = "400";
              $data_pin_scale_height = "240";
              $data_pin_scale_width = "80";
              break;
          }

          $text = '<a data-pin-do="embedBoard" data-pin-board-width="' . $datap_in_board_width . '" data-pin-scale-height="' . $data_pin_scale_height . '" data-pin-scale-width="' . $data_pin_scale_width . '" href="' . $pinterest_board_url . '"></a>';

          $elements[$delta] = [
            '#type' => 'processed_text',
            '#text' => $text,
            '#format' => 'full_html',
          ];
        }
      }
    }
    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->quote));
  }

}
