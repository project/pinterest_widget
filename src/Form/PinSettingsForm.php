<?php

namespace Drupal\pinterest_widget\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Social Autopost.
 */
class PinSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['pinterest_pin.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pinterest_widget.form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pinterest_pin.settings');
    $pin_url = $config->get('pin_url') ?? 'https://www.pinterest.com/pin/99360735500167749/';
    $selected_pin_size = $config->get('pin_size') ?? ['small'];

    $form['advanced_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Default Configurations'),
      '#open' => TRUE,
      '#description' => $this->t('The default values will be auto populated on the fields and blocks.'),
    ];
    $form['advanced_settings']['pin_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Pin URL'),
      '#default_value' => $pin_url,
      '#description' => $this->t('Pin URL. For eg. https://www.pinterest.com/pin/99360735500167749/'),
    ];

    $pin_sizes = [
      'small' => $this->t("Small"),
      'medium' => $this->t("Medium"),
      'large' => $this->t("Large"),
    ];

    $form['advanced_settings']['pin_size'] = [
      '#type' => 'select',
      '#options' => $pin_sizes,
      '#title' => $this->t('Pin size'),
      '#default_value' => $selected_pin_size,
      '#description' => $this->t('Select available opions for user subscription'),
    ];

    $form['advanced_settings']['hide_description'] = [
      '#type' => 'checkbox',
      '#required' => TRUE,
      '#title' => $this->t('Hide description'),
      '#default_value' => $config->get('hide_description') ?? FALSE,
      '#description' => $this->t('Hide Description'),
    ];

    $form['advanced_settings']['preview'] = [
      "#title" => $this->t('Preview'),
      "#type" => "item" ,
      "#markup" => '<a id="follow" data-pin-do="buttonFollow" href="https://www.pinterest.com/pinterest/">Pinterest</a>',
    ];

    $form['advanced_settings']['preview'] = [
      "#title" => $this->t('Preview'),
      "#type" => "item" ,
      "#markup" => '<a id="pin" data-pin-width="' . $selected_pin_size . '" data-pin-do="embedPin" href="' . $pin_url . '"></a>',
    ];

    $form['#attached']['library'][] = 'pinterest_widget/pinterest_widget';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('pinterest_pin.settings')
      ->set('pin_url', $values['pin_url'])
      ->set('pin_size', $values['pin_size'])
      ->set('hide_description', $values['hide_description'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
