<?php

namespace Drupal\pinterest_widget\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Social Autopost.
 */
class FollowSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['pinterest_follow.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pinterest_widget.form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pinterest_follow.settings');
    $pinterest_user_url = $config->get('pinterest_user_url') ?? 'https://www.pinterest.com/pinterest/';
    $full_name = $config->get('full_name') ?? 'Pinterest';

    $form['advanced_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Default Configurations'),
      '#open' => TRUE,
      '#description' => $this->t('The default values will be auto populated on the fields and blocks.'),
    ];

    $form['advanced_settings']['pinterest_user_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Pinterest user URL'),
      '#default_value' => $pinterest_user_url,
      '#description' => $this->t('Pinterest profile url. For eg. https://www.pinterest.com/pinterest/'),
    ];

    $form['advanced_settings']['full_name'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Full name'),
      '#default_value' => $full_name,
      '#description' => $this->t('Pinterest profle name. For eg. Pinterest'),
    ];

    $form['advanced_settings']['preview'] = [
      "#title" => $this->t('Preview'),
      "#type" => "item" ,
      "#markup" => '<a id="follow" data-pin-do="buttonFollow" href="' . $pinterest_user_url . '">' . $full_name . '</a>',
    ];

    $form['#attached']['library'][] = 'pinterest_widget/pinterest_widget';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('pinterest_follow.settings')
      ->set('pinterest_user_url', $values['pinterest_user_url'])
      ->set('full_name', $values['full_name'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
