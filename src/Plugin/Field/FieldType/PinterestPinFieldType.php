<?php

namespace Drupal\pinterest_widget\Plugin\Field\FieldType;

use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'pinterest_pin_fieldtype' field type.
 */
#[FieldType(
  id: "pinterest_pin_fieldtype",
  label: new TranslatableMarkup("Pinterest Pin Field"),
  description: new TranslatableMarkup("Pinterest Pin Field"),
  default_widget: "pinterest_pin_widget",
  default_formatter: "pinterest_pin_formatter"
)]
class PinterestPinFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'is_ascii' => FALSE,
      'case_sensitive' => FALSE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['pin_url'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Pin URL'))
      ->setRequired(FALSE);

    $properties['pin_size'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Pin size'))
      ->setRequired(FALSE);

    $properties['hide_description'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Hide description'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'pin_url' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'pin_size' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'hide_description' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values = [];

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_pin_size, $has_data) {
    $elements = [];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $pin_url = empty($this->get('pin_url')->getValue());
    $pin_size = empty($this->get('pin_size')->getValue());

    return $pin_url || $pin_size;
  }

}
