DESCRIPTION:
------------
The Pinterest widget provides a the Save button to your site’s images, your customers can save things straight from your website to Pinterest, allowing even more people to discover your content. With image hover, the Save button appears when somebody hovers over any image on the page. With any image, they can click the button then choose any image from that page to save.
The module provices the custom block and field widget to place the Follow, Pin, Board and Profile section on the site.

INSTALLATION:
-------------
1. Extract the tar.gz into your 'modules' or directory.
2. Install the module at extend' section.
