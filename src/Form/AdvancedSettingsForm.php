<?php

namespace Drupal\pinterest_widget\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Social Autopost.
 */
class AdvancedSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['pinterest_widget_advanced.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pinterest_widget.form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pinterest_widget_advanced.settings');

    $form['advanced_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced Settings'),
      '#open' => TRUE,
    ];
    $form['advanced_settings']['active'] = [
      '#type' => 'checkbox',
      '#title' => $this
        ->t('Enable Pinterest Widget'),
      '#default_value' => $config->get('active') ?? '0',
      '#description' => $this->t('Activate pinterest widget on the site'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('pinterest_widget_advanced.settings')
      ->set('active', $values['active'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
