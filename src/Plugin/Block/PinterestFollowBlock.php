<?php

namespace Drupal\pinterest_widget\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Pinterest Follow' Block.
 *
 * @Block(
 *   id = "pinterest_follow",
 *   admin_label = @Translation("Pinterest Follow"),
 *   category = @Translation("Pinterest Widget"),
 * )
 */
class PinterestFollowBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * PinterestFollowBlock constructor.
   *
   * @param array $configuration
   *   A configuration array.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Ensure the configuration factory is injected through the container.
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();

    $pinterest_user_url = (isset($config['pinterest_user_url'])) ? $config['pinterest_user_url'] : '';
    $full_name = (isset($config['full_name'])) ? $config['full_name'] : '';
    if ($pinterest_user_url != '') {
      $text = '<a data-pin-do="buttonFollow" href="' . $pinterest_user_url . '">' . $full_name . '</a>';
      $result_data = [
        '#type' => 'processed_text',
        '#text' => $text,
        '#format' => 'full_html',
      ];

      return $result_data;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $pinterest_config = $this->configFactory->get('pinterest_follow.settings');
    $pinterest_user_url = $config['pinterest_user_url'] ?? $pinterest_config->get('pinterest_user_url') ?? '';
    $full_name = $config['full_name'] ?? $pinterest_config->get('full_name') ?? '';
    $form += [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Pinterest Follow'),
    ];

    $form['pinterest_user_url'] = [
      '#type' => 'textfield',
      '#default_value' => $pinterest_user_url,
      '#title' => $this->t('Pinterest user URL'),
      '#description' => $this->t('Pinterest profile url. For eg. https://www.pinterest.com/pinterest/'),
    ];
    $form['full_name'] = [
      '#type' => 'textfield',
      '#default_value' => $full_name,
      '#title' => $this->t('Full name'),
      '#description' => $this->t('Pinterest profle name. For eg. Pinterest'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $pinterest_user_url = $form_state->getValue('pinterest_user_url');
    $full_name = $form_state->getValue('full_name');

    $this->setConfigurationValue('pinterest_user_url', $pinterest_user_url);
    $this->setConfigurationValue('full_name', $full_name);
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {

    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

}
