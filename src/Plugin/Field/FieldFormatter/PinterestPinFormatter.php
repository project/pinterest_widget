<?php

namespace Drupal\pinterest_widget\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'pinterest_pin_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "pinterest_pin_formatter",
 *   label = @Translation("Pinterest pin field formatter"),
 *   field_types = {
 *     "pinterest_pin_fieldtype"
 *   }
 * )
 */
class PinterestPinFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_featured_image) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_featured_image);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // The ProcessedText element already handles cache context & tag bubbling.
    // @see \Drupal\filter\Element\ProcessedText::preRenderText()
    foreach ($items as $delta => $item) {
      $values = $item->getValue();

      if (!empty($values)) {
        $pin_url = (isset($values['pin_url'])) ? $values['pin_url'] : '';
        $pin_size = (isset($values['pin_size'])) ? $values['pin_size'] : '';
        $hide_description = (isset($values['hide_description'])) ? $values['hide_description'] : '';
        if ($pin_url != '') {

          $pin_width = '';
          if ($pin_size) {
            $pin_width = 'data-pin-width="' . $pin_size . '"';
          }
          $terse = '';
          if ($hide_description) {
            $terse = 'data-pin-terse="true"';
          }

          $text = '<a data-pin-do="embedPin" ' . $pin_width . ' ' . $terse . ' href="' . $pin_url . '"></a>';

          $elements[$delta] = [
            '#type' => 'processed_text',
            '#text' => $text,
            '#format' => 'full_html',
          ];
        }
      }
    }
    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->quote));
  }

}
