<?php

namespace Drupal\pinterest_widget\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Pinterest Pin' Block.
 *
 * @Block(
 *   id = "pinterest_pin",
 *   admin_label = @Translation("Pinterest Pin"),
 *   category = @Translation("Pinterest Widget"),
 * )
 */
class PinterestPinBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * PinterestPinBlock constructor.
   *
   * @param array $configuration
   *   A configuration array.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Ensure the configuration factory is injected through the container.
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();

    $pin_url = (isset($config['pin_url'])) ? $config['pin_url'] : '';
    $pin_size = (isset($config['pin_size'])) ? $config['pin_size'] : '';
    $hide_description = (isset($config['hide_description'])) ? $config['hide_description'] : '';
    if ($pin_url != '') {

      $pin_width = '';
      if ($pin_size) {
        $pin_width = 'data-pin-width="' . $pin_size . '"';
      }
      $terse = '';
      if ($hide_description) {
        $terse = 'data-pin-terse="true"';
      }

      $text = '<a data-pin-do="embedPin" ' . $pin_width . ' ' . $terse . ' href="' . $pin_url . '"></a>';

      $result_data = [
        '#type' => 'processed_text',
        '#text' => $text,
        '#format' => 'full_html',
      ];

      return $result_data;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $pinterest_config = $this->configFactory->get('pinterest_pin.settings');
    $pin_url = $pinterest_config->get('pin_url') ?? '';
    $pin_size = $pinterest_config->get('pin_size') ?? '';
    $hide_description = $pinterest_config->get('hide_description') ?? '';

    $pin_url = $config['pin_url'] ?? $pin_url;
    $pin_size = $config['pin_size'] ?? $pin_size;
    $hide_description = $config['hide_description'] ?? $hide_description;

    $form += [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Pinterest Pin'),
    ];

    $pin_sizes = [
      'small' => "Small",
      'medium' => "Medium",
      'large' => "Large",
    ];

    $form['pin_url'] = [
      '#type' => 'textfield',
      '#default_value' => $pin_url,
      '#title' => $this->t('Pin URL'),
      '#description' => $this->t('Pin URL. For eg. https://www.pinterest.com/pin/99360735500167749/'),
    ];
    $form['pin_size'] = [
      '#type' => 'select',
      '#options' => $pin_sizes,
      '#title' => $this->t('Pin size'),
      '#default_value' => $pin_size,
      '#description' => $this->t('Select available opions for user subscription'),
    ];
    $form['hide_description'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide description'),
      '#default_value' => $hide_description,
      '#description' => $this->t('Hide Description'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    parent::blockSubmit($form, $form_state);
    $pin_url = $form_state->getValue('pin_url');
    $pin_size = $form_state->getValue('pin_size');
    $hide_description = $form_state->getValue('hide_description');

    $this->setConfigurationValue('pin_url', $pin_url);
    $this->setConfigurationValue('pin_size', $pin_size);
    $this->setConfigurationValue('hide_description', $hide_description);
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {

    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

}
