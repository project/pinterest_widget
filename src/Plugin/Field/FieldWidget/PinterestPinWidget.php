<?php

namespace Drupal\pinterest_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'pinterest_pin_widget' widget.
 *
 * @FieldWidget(
 *   id = "pinterest_pin_widget",
 *   label = @Translation("Pinterest Pin Default Widget"),
 *   field_types = {
 *     "pinterest_pin_fieldtype"
 *   }
 * )
 */
class PinterestPinWidget extends WidgetBase {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin ID for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ConfigFactoryInterface $config_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('config.factory'),
      );
  }

  /**
   * Gets the initial values for the widget.
   *
   * This is a replacement for the disabled default values functionality.
   *
   * @return array
   *   The initial values, keyed by property.
   */
  protected function getInitialValues() {
    $initial_values = [
      'pin_url' => '',
      'pin_size' => '',
      'hide_description' => '',
    ];

    return $initial_values;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_featured_image) {
    $item = $items[$delta];
    $value = $item->getEntity()->isNew() ? $this->getInitialValues() : $item->toArray();

    $config = $this->configFactory->get('pinterest_pin.settings');
    $pin_url = $config->get('pin_url') ?? '';
    $pin_size = $config->get('pin_size') ?? '';
    $hide_description = $config->get('hide_description') ?? '';

    $pin_url = $value['pin_url'] ?? $pin_url;
    $pin_size = $value['pin_size'] ?? $pin_size;
    $hide_description = $value['hide_description'] ?? $hide_description;

    $element += [
      '#type' => 'details',
      '#open' => TRUE,
    ];

    $pin_sizes = [
      'small' => "Small",
      'medium' => "Medium",
      'large' => "Large",
    ];

    $element['pin_url'] = [
      '#type' => 'textfield',
      '#default_value' => $pin_url,
      '#title' => $this->t('Pin URL'),
      '#description' => $this->t('Pin URL. For eg. https://www.pinterest.com/pin/99360735500167749/'),
    ];
    $element['pin_size'] = [
      '#type' => 'select',
      '#options' => $pin_sizes,
      '#title' => $this->t('Pin size'),
      '#default_value' => $pin_size,
      '#description' => $this->t('Select available opions for user subscription'),
    ];
    $element['hide_description'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide description'),
      '#default_value' => $hide_description,
      '#description' => $this->t('Hide Description'),
    ];

    return $element;
  }

}
