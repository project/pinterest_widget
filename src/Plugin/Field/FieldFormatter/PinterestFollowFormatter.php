<?php

namespace Drupal\pinterest_widget\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'pinterest_follow_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "pinterest_follow_formatter",
 *   label = @Translation("Pinterest follow field formatter"),
 *   field_types = {
 *     "pinterest_follow_fieldtype"
 *   }
 * )
 */
class PinterestFollowFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_featured_image) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_featured_image);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    // The ProcessedText element already handles cache context & tag bubbling.
    // @see \Drupal\filter\Element\ProcessedText::preRenderText()
    foreach ($items as $delta => $item) {
      $values = $item->getValue();

      if (!empty($values)) {
        $pinterest_user_url = (isset($values['pinterest_user_url'])) ? $values['pinterest_user_url'] : '';
        $full_name = (isset($values['full_name'])) ? $values['full_name'] : '';
        if ($pinterest_user_url != '') {
          $text = '<a data-pin-do="buttonFollow" href="' . $pinterest_user_url . '">' . $full_name . '</a>';
          $elements[$delta] = [
            '#type' => 'processed_text',
            '#text' => $text,
            '#format' => 'full_html',
          ];
        }
      }
    }
    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->quote));
  }

}
