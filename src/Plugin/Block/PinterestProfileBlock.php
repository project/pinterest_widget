<?php

namespace Drupal\pinterest_widget\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Pinterest Profile' Block.
 *
 * @Block(
 *   id = "pinterest_profile",
 *   admin_label = @Translation("Pinterest Profile"),
 *   category = @Translation("Pinterest Widget"),
 * )
 */
class PinterestProfileBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * PinterestProfileBlock constructor.
   *
   * @param array $configuration
   *   A configuration array.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // Ensure the configuration factory is injected through the container.
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();

    $pinterest_user_url = (isset($config['pinterest_user_url'])) ? $config['pinterest_user_url'] : '';

    if ($pinterest_user_url != '') {
      $size = (isset($config['size'])) ? $config['size'] : '';

      switch ($size) {
        case 'sidebar':
          $datap_in_board_width = "150";
          $data_pin_scale_height = "800";
          $data_pin_scale_width = "60";

          break;

        case 'header':
          $datap_in_board_width = "900";
          $data_pin_scale_height = "120";
          $data_pin_scale_width = "115";

          break;

        case 'custom':
          $datap_in_board_width = (isset($config['image_width'])) ? $config['image_width'] : '400';
          $data_pin_scale_height = (isset($config['board_height'])) ? $config['board_height'] : '240';
          $data_pin_scale_width = (isset($config['board_width'])) ? $config['board_width'] : '80';
          break;

        default:
          $datap_in_board_width = "400";
          $data_pin_scale_height = "240";
          $data_pin_scale_width = "80";
          break;
      }

      $text = '<a data-pin-do="embedUser" data-pin-board-width="' . $datap_in_board_width . '" data-pin-scale-height="' . $data_pin_scale_height . '" data-pin-scale-width="' . $data_pin_scale_width . '" href="' . $pinterest_user_url . '"></a>';

      $result_data = [
        '#type' => 'processed_text',
        '#text' => $text,
        '#format' => 'full_html',
      ];

      return $result_data;

    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $pinterest_config = $this->configFactory->get('pinterest_profile.settings');
    $config_pinterest_user_url = $pinterest_config->get('pinterest_user_url') ?? '';
    $config_size = $pinterest_config->get('size') ?? '';
    $config_image_width = $pinterest_config->get('image_width') ?? '';
    $config_board_height = $pinterest_config->get('board_height') ?? '';
    $config_board_width = $pinterest_config->get('board_width') ?? '';

    if ($count_items > 1) {
      $config_pinterest_user_url = '';
      $config_image_width = '';
      $config_board_height = '';
      $config_board_width = '';
    }

    $pinterest_user_url = $config['pinterest_user_url'] ?? $config_pinterest_user_url;
    $size = $config['size'] ?? $config_size;
    $image_width = $config['image_width'] ?? $config_image_width;
    $board_height = $config['board_height'] ?? $config_board_height;
    $board_width = $config['board_width'] ?? $config_board_width;

    $form += [
      '#title' => $this->t('Pinterest Profile'),
      '#type' => 'details',
      '#open' => TRUE,
    ];

    $field_index = "settings[size]";

    $form['pinterest_user_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pinterest user URL'),
      '#default_value' => $pinterest_user_url,
      '#description' => $this->t('Pin URL. For eg. https://www.pinterest.com/pinterest/'),
    ];

    $sizes = [
      'square' => 'Square',
      'sidebar' => 'Sidebar',
      'header' => 'Header',
      'custom' => 'Create',
    ];

    $form['size'] = [
      '#type' => 'select',
      '#options' => $sizes,
      '#title' => $this->t('Size'),
      '#default_value' => $size,
      '#description' => $this->t('Select available opions for user subscription'),
    ];

    $form['image_width'] = [
      '#type' => 'number',
      '#title' => $this->t('Image width'),
      '#default_value' => $image_width,
      '#states' => [
        'visible' => [
          ':input[name="' . $field_index . '"]' => [
            'value' => 'custom',
          ],
        ],
      ],
      '#description' => $this->t('The default Image width is 80'),
    ];
    $form['board_height'] = [
      '#type' => 'number',
      '#title' => $this->t('Board height'),
      '#default_value' => $board_height,
      '#states' => [
        'visible' => [
          ':input[name="' . $field_index . '"]' => [
            'value' => 'custom',
          ],
        ],
      ],
      '#description' => $this->t('The default Board height is 240'),
    ];
    $form['board_width'] = [
      '#type' => 'number',
      '#title' => $this->t('Board width'),
      '#default_value' => $board_width,
      '#states' => [
        'visible' => [
          ':input[name="' . $field_index . '"]' => [
            'value' => 'custom',
          ],
        ],
      ],
      '#description' => $this->t('The default Board width is 400'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $pinterest_user_url = $form_state->getValue('pinterest_user_url');
    $size = $form_state->getValue('size');
    $image_width = $form_state->getValue('image_width');
    $board_height = $form_state->getValue('board_height');
    $board_width = $form_state->getValue('board_width');

    $this->setConfigurationValue('pinterest_user_url', $pinterest_user_url);
    $this->setConfigurationValue('size', $size);
    $this->setConfigurationValue('image_width', $image_width);
    $this->setConfigurationValue('board_height', $board_height);
    $this->setConfigurationValue('board_width', $board_width);
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {

    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

}
