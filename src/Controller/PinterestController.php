<?php

namespace Drupal\pinterest_widget\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Metatag routes.
 */
class PinterestController extends ControllerBase {

  /**
   * Provides help information for the Pinterest widget.
   *
   * This function retrieves the help information from the
   * pinterest_widget_get_pin_help_info()`
   * function and returns it as a renderable array with the translated text.
   *
   * @param string $value
   *   (optional) A value passed to the function. This parameter is
   *   currently unused but
   *   could be used for customization in the future.
   *
   * @return array
   *   A renderable array containing the help information text, ready
   *   to be displayed on the page.
   */
  public function help($value = '') {
    $pinterest_widget_get_pin_help_info = pinterest_widget_get_pin_help_info();
    $build = [
      '#markup' => $this->t('@help_info', ['@help_info' => $pinterest_widget_get_pin_help_info]),
    ];
    return $build;
  }

  /**
   * Provides information about the Pinterest widget.
   *
   * This function retrieves the information about the Pinterest widget from
   * the `pinterest_widget_about()` function and returns it as a renderable
   * array with the translated text.
   *
   * @param string $value
   *   (optional) A value passed to the function. This parameter is
   *   currently unused but could be used for customization in the future.
   *
   * @return array
   *   A renderable array containing the about information text, ready to
   *   be displayed on the page.
   */
  public function about($value = '') {
    $about_pinterest = pinterest_widget_about();
    $build = [
      '#markup' => $this->t('@about_pinterest', ['@about_pinterest' => $about_pinterest]),
    ];
    return $build;
  }

}
