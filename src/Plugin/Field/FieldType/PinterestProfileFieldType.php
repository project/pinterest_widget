<?php

namespace Drupal\pinterest_widget\Plugin\Field\FieldType;

use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'pinterest_profile_fieldtype' field type.
 */
#[FieldType(
  id: "pinterest_profile_fieldtype",
  label: new TranslatableMarkup("Pinterest Profile"),
  description: new TranslatableMarkup("Pinterest Profile Field"),
  default_widget: "pinterest_profile_widget",
  default_formatter: "pinterest_profile_formatter"
)]
class PinterestProfileFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'is_ascii' => FALSE,
      'case_sensitive' => FALSE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['pinterest_user_url'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Pinterest user URL'))
      ->setRequired(FALSE);

    $properties['size'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Size'))
      ->setRequired(FALSE);

    $properties['image_width'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Image width'))
      ->setRequired(FALSE);

    $properties['board_height'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Board height'))
      ->setRequired(FALSE);

    $properties['board_width'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Board width'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'pinterest_user_url' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'size' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'image_width' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'board_height' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'board_width' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values = [];

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_size, $has_data) {
    $elements = [];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $pinterest_user_url = empty($this->get('pinterest_user_url')->getValue());
    $size = empty($this->get('size')->getValue());

    return $pinterest_user_url || $size;
  }

}
