<?php

namespace Drupal\pinterest_widget\Plugin\Field\FieldType;

use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'pinterest_follow_fieldtype' field type.
 */
#[FieldType(
  id: "pinterest_follow_fieldtype",
  label: new TranslatableMarkup("Pinterest Follow Field"),
  description: new TranslatableMarkup("Pinterest Follow Field"),
  default_widget: "pinterest_follow_widget",
  default_formatter: "pinterest_follow_formatter"
)]
class PinterestFollowFieldType extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'is_ascii' => FALSE,
      'case_sensitive' => FALSE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['pinterest_user_url'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Pinterest user URL'))
      ->setRequired(FALSE);

    $properties['full_name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Full name'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'pinterest_user_url' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'full_name' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values = [];

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_full_name, $has_data) {
    $elements = [];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $pinterest_user_url = empty($this->get('pinterest_user_url')->getValue());
    $full_name = empty($this->get('full_name')->getValue());

    return $pinterest_user_url || $full_name;
  }

}
