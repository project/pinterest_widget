<?php

namespace Drupal\pinterest_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'pinterest_follow_widget' widget.
 *
 * @FieldWidget(
 *   id = "pinterest_follow_widget",
 *   label = @Translation("Pinterest Follow Default Widget"),
 *   field_types = {
 *     "pinterest_follow_fieldtype"
 *   }
 * )
 */
class PinterestFollowWidget extends WidgetBase {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin ID for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ConfigFactoryInterface $config_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('config.factory'),
      );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => 60,
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * Gets the initial values for the widget.
   *
   * This is a replacement for the disabled default values functionality.
   *
   * @return array
   *   The initial values, keyed by property.
   */
  protected function getInitialValues() {
    $initial_values = [
      'pinterest_user_url' => '',
      'full_name' => '',
    ];

    return $initial_values;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_featured_image) {
    $item = $items[$delta];
    $value = $item->getEntity()->isNew() ? $this->getInitialValues() : $item->toArray();

    $config = $this->configFactory->get('pinterest_follow.settings');
    $pinterest_user_url = $config->get('pinterest_user_url') ?? '';
    $full_name = $config->get('full_name') ?? '';

    $pinterest_user_url = $value['pinterest_user_url'] ?? $pinterest_user_url;
    $full_name = $value['full_name'] ?? $full_name;

    $element += [
      '#type' => 'details',
      '#open' => TRUE,
    ];

    $element['pinterest_user_url'] = [
      '#type' => 'textfield',
      '#default_value' => $pinterest_user_url,
      '#title' => $this->t('Pinterest user URL'),
      '#description' => $this->t('Pinterest profile url. For eg. https://www.pinterest.com/pinterest/'),
    ];
    $element['full_name'] = [
      '#type' => 'textfield',
      '#default_value' => $full_name,
      '#title' => $this->t('Full name'),
      '#description' => $this->t('Pinterest profle name. For eg. Pinterest'),
    ];

    return $element;
  }

}
