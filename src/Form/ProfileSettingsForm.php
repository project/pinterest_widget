<?php

namespace Drupal\pinterest_widget\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings form for Social Autopost.
 */
class ProfileSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['pinterest_profile.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pinterest_widget.form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('pinterest_profile.settings');
    $pinterest_user_url = $config->get('pinterest_user_url') ?? "https://www.pinterest.com/pinterest/";
    $data_pin_board_width = $config->get('image_width') ?? "80";
    $data_pin_scale_height = $config->get('board_height') ?? "240";
    $data_pin_scale_width = $config->get('board_width') ?? "400";

    $form['advanced_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Default Configurations'),
      '#open' => TRUE,
      '#description' => $this->t('The default values will be auto populated on the fields and blocks.'),
    ];

    $form['advanced_settings']['pinterest_user_url'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Pinterest user URL'),
      '#default_value' => $pinterest_user_url,
      '#description' => $this->t('Pin URL. For eg. https://www.pinterest.com/pinterest/'),
    ];

    $sizes = [
      "square" => "Square",
      "sidebar" => "Sidebar",
      "header" => "Header",
      "custom" => "Create your own",
    ];
    $selected_size = $config->get('size') ?? [];

    $form['advanced_settings']['size'] = [
      '#type' => 'select',
      '#options' => $sizes,
      '#title' => $this->t('Size'),
      '#default_value' => $selected_size,
      '#description' => $this->t('Select available opions for user subscription'),
    ];

    $form['advanced_settings']['image_width'] = [
      '#type' => 'number',
      '#required' => TRUE,
      '#title' => $this->t('Image width'),
      '#default_value' => $data_pin_board_width,
      '#states' => [
    // Only show this field when the 'toggle_me' checkbox is enabled.
        'visible' => [
          ':input[name="size"]' => [
            'value' => 'custom',
          ],
        ],
      ],
      '#description' => $this->t('The default Image width is 80'),
    ];
    $form['advanced_settings']['board_height'] = [
      '#type' => 'number',
      '#required' => TRUE,
      '#title' => $this->t('Board height'),
      '#default_value' => $data_pin_scale_height,
      '#states' => [
    // Only show this field when the 'toggle_me' checkbox is enabled.
        'visible' => [
          ':input[name="size"]' => [
            'value' => 'custom',
          ],
        ],
      ],
      '#description' => $this->t('The default Board height is 240'),
    ];
    $form['advanced_settings']['board_width'] = [
      '#type' => 'number',
      '#required' => TRUE,
      '#title' => $this->t('Board width'),
      '#default_value' => $data_pin_scale_width,
      '#states' => [
    // Only show this field when the 'toggle_me' checkbox is enabled.
        'visible' => [
          ':input[name="size"]' => [
            'value' => 'custom',
          ],
        ],
      ],
      '#description' => $this->t('The default Board width is 400'),
    ];

    $custom_sizes = '';
    if ($selected_size == 'custom') {
      $custom_sizes = 'data-pin-board-width="' . $data_pin_scale_width . '" data-pin-scale-height="' . $data_pin_scale_height . '" data-pin-scale-width="' . $data_pin_board_width . '"';
    }

    $form['advanced_settings']['preview'] = [
      "#title" => $this->t('Preview'),
      "#type" => "item" ,
      "#markup" => '<a data-pin-do="embedUser" ' . $custom_sizes . ' href="' . $pinterest_user_url . '"></a>',
    ];

    $form['#attached']['library'][] = 'pinterest_widget/pinterest_widget';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('pinterest_profile.settings')
      ->set('pinterest_user_url', $values['pinterest_user_url'])
      ->set('size', $values['size'])
      ->set('image_width', $values['image_width'])
      ->set('board_height', $values['board_height'])
      ->set('board_width', $values['board_width'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
