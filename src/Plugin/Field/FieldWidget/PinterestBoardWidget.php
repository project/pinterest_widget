<?php

namespace Drupal\pinterest_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Plugin implementation of the 'pinterest_board_widget' widget.
 *
 * @FieldWidget(
 *   id = "pinterest_board_widget",
 *   label = @Translation("Pinterest Board Default Widget"),
 *   field_types = {
 *     "pinterest_board_fieldtype"
 *   }
 * )
 */
class PinterestBoardWidget extends WidgetBase {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin ID for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ConfigFactoryInterface $config_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('config.factory'),
      );
  }

  /**
   * Gets the initial values for the widget.
   *
   * This is a replacement for the disabled default values functionality.
   *
   * @return array
   *   The initial values, keyed by property.
   */
  protected function getInitialValues() {
    $initial_values = [
      'pinterest_board_url' => '',
      'size' => '',
      'image_width' => '',
      'board_height' => '',
      'board_width' => '',
    ];

    return $initial_values;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_featured_image) {
    $item = $items[$delta];

    $count_items = $items->count();

    $value = [];

    $value = $item->getEntity()->isNew() ? $this->getInitialValues() : $item->toArray();

    $config = $this->configFactory->get('pinterest_board.settings');
    $config_pinterest_board_url = $config->get('pinterest_board_url') ?? '';
    $config_image_width = $config->get('image_width') ?? '';
    $config_board_height = $config->get('board_height') ?? '';
    $config_board_width = $config->get('board_width') ?? '';

    $delta_count = '';
    if ($count_items > 1) {
      $config_pinterest_board_url = '';
      $config_image_width = '';
      $config_board_height = '';
      $config_board_width = '';
      $delta_count = $delta;
    }

    $pinterest_board_url = $value['pinterest_board_url'] ?? $config_pinterest_board_url;
    $size = $value['size'] ?? $size;
    $image_width = $value['image_width'] ?? $config_image_width;
    $board_height = $value['board_height'] ?? $config_board_height;
    $board_width = $value['board_width'] ?? $config_board_width;

    $element = [];

    $element += [
      '#title' => $this->t('Pinterest Board @delta_count', ['@delta_count' => $delta_count]),
      '#type' => 'details',
      '#open' => TRUE,
    ];

    $field_index = "field_pinterest_board[" . $delta . "][size]";

    $element['pinterest_board_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pinterest board URL'),
      '#default_value' => $pinterest_board_url,
      '#description' => $this->t('Pin URL. For eg. https://www.pinterest.com/pinterest/official-news/'),
    ];

    $sizes = [
      'square' => 'Square',
      'sidebar' => 'Sidebar',
      'header' => 'Header',
      'custom' => 'Create',
    ];

    $element['size'] = [
      '#type' => 'select',
      '#options' => $sizes,
      '#title' => $this->t('Size'),
      '#default_value' => $size,
      '#description' => $this->t('Select available opions for user subscription'),
    ];

    $element['image_width'] = [
      '#type' => 'number',
      '#title' => $this->t('Image width'),
      '#default_value' => $image_width,
      '#states' => [
        'visible' => [
          ':input[name="' . $field_index . '"]' => [
            'value' => 'custom',
          ],
        ],
      ],
      '#description' => $this->t('The default Image width is 80'),
    ];
    $element['board_height'] = [
      '#type' => 'number',
      '#title' => $this->t('Board height'),
      '#default_value' => $board_height,
      '#states' => [
        'visible' => [
          ':input[name="' . $field_index . '"]' => [
            'value' => 'custom',
          ],
        ],
      ],
      '#description' => $this->t('The default Board height is 240'),
    ];
    $element['board_width'] = [
      '#type' => 'number',
      '#title' => $this->t('Board width'),
      '#default_value' => $board_width,
      '#states' => [
        'visible' => [
          ':input[name="' . $field_index . '"]' => [
            'value' => 'custom',
          ],
        ],
      ],
      '#description' => $this->t('The default Board width is 400'),
    ];

    return $element;
  }

}
